# Legacy

This is a fork of the Antü icon theme for Plasma 5, created by Fabian Inostroza. 
In this fork I include some personal icons based in the general style of the theme. 
Some minor issues are solved in order to guaranty consistence with Plasma 5.

## Installation

After download and the uncompressing process, copy the three folders into `/home/your_user/.local/usr/icons` or into `/usr/share/icons/`. Then select the icon
theme in your system settings. 

## Contact

If you have any comment or observation, please write me to vmorenomarin@gmail.com.

## Review

![]
(http://i.imgur.com/Mi2QOKD.jpg)
